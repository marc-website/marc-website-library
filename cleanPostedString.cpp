// cleanPostedString.cpp :
//
// Includes:
//   cleanPostedString - Convert invalid characters in posted string
//   translatePost - Translate %nn strings in posted string

#include <stdio.h>
#include <string.h>

// Functions included
void cleanPostedString( char * );
void translatePost( char * );

void cleanPostedString( char *p )
{
  char *q;
  char szWork[4096];

  memset(szWork,0,sizeof(szWork));
  strcpy(szWork,p);                // Copy initial string to work area
  q = szWork;                      // Point to beginning of work area
  while ( *q )
  {
    if ( *q < ' ' )                // Nonprintable, make it a blank
      *p = ' ';
    else if ( *p == '\'' )         // Apostrophe, escape it
      {
      *p = '\\';
      p++;
      *p = '\'';
      }
    else if ( *p == '\\' )         // Backslash, escape it
      {
      *p = '\\';
      p++;
      *p = '\\';
      }
    else                           // Anything else just copy
      *p = *q;
    q++;
    p++;
  }
}


void translatePost( char *szText )
{
  char szWork[1024], szWork1[32];
  char *p,*q;

  memset(szWork,0,sizeof(szWork));

  p = szText;
  q = szWork;
  while ( *p )
    {
      if ( (*p=='+') )
	{
	  *q = ' ';
	  p++;
	  q++;
	}
      else if ( *p == '%' )
	{
	  memset(szWork1,0,sizeof(szWork1));
	  p++;
	  memcpy(szWork1,p,2);
	  sscanf(szWork1,"%x",(unsigned int *)q);  // Yes, it's really a %x
	  if ( *q == ',' )
	    *q = ' ';
	  if ( *q < ' ' )
	    *q = ' ';
	  p+=2;
	  q++;
	}
    else
      {
	*q = *p;
	p++;
	q++;
      }
    }
  strcpy(szText,szWork);
}










