// dateSube.cpp
//
// Date functions used by MARC database programs

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

char szMonths[12][4] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

// formatDate - convert a date from yyyy-mm-dd (ISO) format
//              to mmm dd used as abbreviated date int
//              MARC web pages where year is known

void formatDate( char *szDate )
{
   int nM, nD, nY;
   char szWork[32];

   sscanf(szDate,"%d-%d-%d",&nY,&nM,&nD);
   sprintf(szWork,"%s %d",szMonths[nM-1],nD);
   strcpy(szDate,szWork);
}

// todaysDate - return the current date as a string of
//              the form yy/mm/dd

void todaysDate( char *szDate )
{
  time_t ltime;
  char szWork[128];
  char szDummy[32],szMonth[32];
  int nYr, nMo, nDa;
  int i;

  time(&ltime);
  strcpy(szWork,ctime(&ltime));
  sscanf(szWork,"%s %s %d %s %d",szDummy,szMonth,&nDa,szDummy,&nYr);
  nYr %= 100;
  nMo = 0;
  for ( i=0; i<12; i++ )
    if ( !strcmp(szMonths[i],szMonth) )
      nMo = i+1;
  sprintf(szDate,"%02d/%d/%d",nYr,nMo,nDa);
}
