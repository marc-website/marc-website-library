/*! \file exitNicely.cpp

 \brief Provide a clean exit from program

 This function is called by many of the MARC database routines in
 the event of an error.  It closes the database connection, closes
 the HTML stream, and flushes stdout.
*/

#include <stdio.h>
#include <stdlib.h>
#include "/usr/include/libpq-fe.h"

//! Open connection to the database
extern PGconn *conn;

//! exitNicely - cleanly exit from program
/*! exitNicely() closes the database connection, then emits
    (on stdout) HTML code to close the page, and flushes both
    stdout and stderr.  Finally, it exits the application with
    a failure error code.

    \b Arguments - none
    \return EXIT_FAILURE from application
*/
void exitNicely( )
{
  PQfinish(conn);
  printf("</BODY>\n");
  printf("</HEAD>\n");
  fflush(stdout);
  fflush(stderr);
  exit(EXIT_FAILURE);
}

