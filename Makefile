CC=gcc
CPP=g++
CFLAGS= -g -Wall -c
TARGDIR=/var/www/cgi-bin
LIB_DEPS=dateSubs.o cleanString.o exitNicely.o Transaction.o htmlUtils.o makeConn.o cleanPostedString.o HTMLsubs.o
MM_DEPS=MARCmkMeet.o MARC.a
MN_DEPS=MARCupdNet.o MARC.a
MC_DEPS=MARCclubMeet.o MARC.a
M3_DEPS=MARCclubMeet2.o MARC.a
M4_DEPS=MARCclubMeet3.o MARC.a
MA_DEPS=MARCaddMeet.o MARC.a
ML_DEPS=MARCaddMeetL.o MARC.a
M2_DEPS=MARCaddMeet2.o MARC.a
TH_DEPS=MARCthursday.o
WD_DEPS=wday.o
MB_DEPS=MARCmkBull.o MARC.a
MS_DEPS=MARCmkSearch.o MARC.a ../../Greenleaf/Greenleaf.a

.c.o:
	$(CC) $(CFLAGS) $<

.cpp.o:
	$(CPP) $(CFLAGS) $<

all: MARCmkMeet.exe MARCaddMeet.cgi MARCaddMeet2.cgi MARCmkBull.exe MARCclubMeet.cgi MARCclubMeet2.cgi \
	MARCclubMeet3.cgi MARCupdNet.cgi MARCaddMeetL.cgi MARCmkSearch MARCthursday

MARCthursday : $(TH_DEPS)
	$(CPP) $(TH_DEPS) -o $@

MARCmkSearch : $(MS_DEPS)
	$(CPP) $(MS_DEPS) -o $@ -lpq -lcrypt

MARCmkMeet.exe : $(MM_DEPS)
	$(CPP) $(MM_DEPS) -o $@ -lpq -lcrypt

MARCmkBull.exe : $(MB_DEPS)
	$(CPP) $(MB_DEPS) -o $@ -lpq -lcrypt

wday : $(WD_DEPS)
	$(CPP) $(WD_DEPS) -o $@

MARC.a : $(LIB_DEPS)
	ar -r $@ $(LIB_DEPS)

MARCupdNet.cgi : $(MN_DEPS)
	$(CPP) $(MN_DEPS) -o $@ -lpq -lcrypt

MARCclubMeet.cgi : $(MC_DEPS)
	$(CPP) $(MC_DEPS) -o $@ -lpq -lcrypt

MARCclubMeet3.cgi : $(M4_DEPS)
	$(CPP) $(M4_DEPS) -o $@ -lpq -lcrypt

MARCclubMeet2.cgi : $(M3_DEPS)
	$(CPP) $(M3_DEPS) -o $@ -lpq -lcrypt

MARCaddMeetL.cgi : $(ML_DEPS)
	$(CPP) $(ML_DEPS) -o $@ -lpq -lcrypt

MARCaddMeet.cgi : $(MA_DEPS)
	$(CPP) $(MA_DEPS) -o $@ -lpq -lcrypt

MARCaddMeet2.cgi : $(M2_DEPS)
	$(CPP) $(M2_DEPS) -o $@ -lpq -lcrypt

$(TARGDIR)/MARCclubMeet.cgi : MARCclubMeet.cgi
	cp $< $(TARGDIR)
	chmod +x $@
	chown nobody $@
	chgrp nobody $@

$(TARGDIR)/MARCclubMeet2.cgi : MARCclubMeet2.cgi
	cp $< $(TARGDIR)
	chmod +x $@
	chown nobody $@
	chgrp nobody $@

$(TARGDIR)/MARCclubMeet3.cgi : MARCclubMeet3.cgi
	cp $< $(TARGDIR)
	chmod +x $@
	chown nobody $@
	chgrp nobody $@

$(TARGDIR)/MARCaddMeetL.cgi : MARCaddMeetL.cgi
	cp $< $(TARGDIR)
	chmod +x $@
	chown nobody $@
	chgrp nobody $@

$(TARGDIR)/MARCaddMeet.cgi : MARCaddMeet.cgi
	cp $< $(TARGDIR)
	chmod +x $@
	chown nobody $@
	chgrp nobody $@

$(TARGDIR)/MARCaddMeet2.cgi : MARCaddMeet2.cgi
	cp $< $(TARGDIR)
	chmod +x $@
	chown nobody $@
	chgrp nobody $@

$(TARGDIR)/MARCupdNet.cgi : MARCupdNet.cgi
	cp $< $(TARGDIR)
	chmod +x $@
	chown nobody $@
	chgrp nobody $@

install: MARCmkMeet.exe $(TARGDIR)/MARCaddMeet.cgi $(TARGDIR)/MARCaddMeet2.cgi \
	$(TARGDIR)/MARCclubMeet.cgi $(TARGDIR)/MARCclubMeet2.cgi $(TARGDIR)/MARCclubMeet3.cgi \
	$(TARGDIR)/MARCupdNet.cgi $(TARGDIR)/MARCaddMeetL.cgi MARCmkBull.exe
	echo Installed

html : MARCmkMeet.exe
	./MARCmkMeet.exe
	cp Meetings.html /home/httpd/html/MARC




