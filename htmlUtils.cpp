// htmlUtils.cpp :
//
// Provides header and trailer for HTML pages.
//
// Included functions:
//    htmlOpen
//    htmlClose

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// htmlOpen - output the HTML header to stdout

void htmlOpen( FILE *f, char *szPageTitle, char *szKeywords, char *szBaseHref )
{
    fprintf(f,"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40\">\n");
    fprintf(f,"<HTML lang=\"en\">\n");
    fprintf(f,"<head>\n");
    fprintf(f,"	<title>%s</title>\n",szPageTitle);
    fprintf(f,"	<META NAME=DESCRIPTION CONTENT=\"Midland Amateur Radio Club\">\n");
    fprintf(f,"	<META NAME=KEYWORDS CONTENT=\"Ham Radio Amateur Radio %s\">\n",szKeywords);
    fprintf(f,"<STYLE TYPE=\"text/css\">\n");
    fprintf(f,"<!--\n");
    fprintf(f,"BODY       { background-color:Cornsilk }\n");
    fprintf(f,"P          { font-family:Times New Roman,Times;color:Black;font-size:14pt }\n");
    fprintf(f,"SPAN.Net   { font-family:Times New Roman,Times;color:#002f9f;font-size:14pt;font-weight:800 }\n");
    fprintf(f,"SPAN.Meet  { font-family:Times New Roman,Times;color:#cf0000;font-size:14pt;font-weight:800 }\n");
    fprintf(f,"SPAN.Note  { font-family:Arial,Helvetica;color:#ff0000;font-size:14pt;font-weight:800 }\n");
    fprintf(f,"SPAN.RACES { font-family:Times New Roman,Times;color:#008f00;font-size:14pt;font-weight:800 }\n");
    fprintf(f,"SPAN.Event { font-family:Times New Roman,Times;color:#6f009f;font-size:14pt;font-weight:800 }\n");
    fprintf(f,"SPAN.Contest{ font-family:Times New Roman,Times;color:#2f0f00;font-size:14pt;font-weight:400 }\n");
    fprintf(f,"SPAN.Novic { font-family:Times New Roman,Times;color:#9f006f;font-size:14pt;font-weight:800 }\n");
    fprintf(f,"SPAN.Fox   { font-family:Times New Roman,Times;color:#3f0f00;font-size:14pt;font-weight:400 }\n");
    fprintf(f,"P.Author   { font-family:Arial,Helvetica;color:#7f7f7f;font-weight:100;font-size:10pt }\n");
    fprintf(f,"U.Link     { color:blue;font-size:12pt }\n");
    fprintf(f,"a:hover    { color:#ff4f4f;text-decoration:underline }\n");
    fprintf(f,"a:active   { color:#000000;text-decoration:underline }\n");
    fprintf(f,"-->\n");
    fprintf(f,"</STYLE>\n");
    fprintf(f,"</head>\n");
    fprintf(f,"\n");
    fprintf(f,"<body background=\"%s/images/screen.gif\" link=#003f00 vlink=SeaGreen bgcolor=Cornsilk>\n",szBaseHref);
    fprintf(f,"\n");
    fprintf(f,"<table width=100%%>\n");
    fprintf(f,"    <tr>\n");
    fprintf(f,"	<td width=15%% align=left><img src=\"%s/images/arrl.gif\"></td>\n",szBaseHref);
    fprintf(f,"\n");
    fprintf(f,"    	<td align=center>\n");
    fprintf(f,"        <font size=+5 color=\"#000000\" face=\"Times New Roman\"><b>M</font><font size=+3 color=\"#FF0000\">idland</font>\n");
    fprintf(f,"	<font size=+5 color=\"#000000\" >A</font><font size=+3 color=\"#FF0000\">mateur</font>\n");
    fprintf(f,"	<font size=+5 color=\"#000000\" >R</font><font size=+3 color=\"#FF0000\">adio</font>\n");
    fprintf(f,"	<font size=+5 color=\"#000000\" >C</font><font size=+3 color=\"#FF0000\">lub</font>\n");
    fprintf(f,"	</td>\n");
    fprintf(f,"\n");
    fprintf(f,"	<td width=15%% align=right><img src=\"%s/images/arrl.gif\"></td>\n",szBaseHref);
    fprintf(f,"    </tr>\n");
    fprintf(f,"</table>\n");
    fprintf(f,"<BR>\n");
    fprintf(f,"\n");
    fprintf(f,"    <center>\n");
    fprintf(f,"        <font size=+5 color=\"#000000\" face=\"Times New Roman\"><b>%s\n",szPageTitle);
    fprintf(f,"	</b></font><br>\n");
    fprintf(f,"\n");
    fprintf(f,"<HR height=3 color=#003f00>\n");
}

// htmlClose - output the HTML closing lines

void htmlClose( FILE *f )
{
    time_t ltime;

    fprintf(f,"\n");
    fprintf(f,"</TABLE>\n");
    fprintf(f,"<P>\n");
    fprintf(f,"\n");
    fprintf(f,"</CENTER>\n");
    fprintf(f,"\n");
    fprintf(f,"<HR height=3 color=#003f00>\n");
    fprintf(f,"<BR>\n");
    fprintf(f,"<P>\n");
    time(&ltime);
    fprintf(f,"<FONT SIZE=-1 color=#000000>Last updated %s</FONT><BR>\n",ctime(&ltime));
    fprintf(f,"</html>\n");
}
