/*! \file makeConn.cpp :

 \brief Make a connection to the MARC database

 Primarily only to concentrate the name etc. of the database
 in one place.
*/
// 07-Dec-01 JJMcD
// 01-Jun-04 JJMcD - Change database to Cormack


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "/usr/include/libpq-fe.h"

/* Included functions */
//! Make a connection to the MARC database
void makeConn( void );

/* Other functions */
void exitNicely( void );

//! Name of the database
#define DBNAME "MIARPSC"
//! Node on which postmaster is running
#define DBNODE "192.68.0.20"

/* Global variables */
extern PGconn *conn;

// makeConn - Connect to the MARC database

/*! makeConn simply executes a PQsetdb() call to the MARC
    database, and generates an HTML-wrapped error message
    if the connection fails.

\b Arguments:
  \arg none
\return none

\b Pseudocode:
\code
conn = PQsetdb()
if PQstatus == CONNECTION_BAD
  print error message
  exitNicely()
\endcode

*/
void makeConn( void )
{
  /* make a connection to the database */
  conn = PQsetdb(DBNODE, NULL, NULL, NULL, DBNAME);
  /*     * check to see that the backend connection was successfully made
   */
  if (PQstatus(conn) == CONNECTION_BAD)
    {
      fprintf(stdout, "<P>Connection to database '%s' on '%s' failed.<BR>\n",
	      DBNAME,DBNODE);
      fprintf(stdout, "%s</P>\n", PQerrorMessage(conn));
      exitNicely();
    }
}


















