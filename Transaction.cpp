// Transaction.cpp :
//
// Contains functions used to begin and end transactions on the
// MARC database and to retreive data:
//
// Included:
//    startTransaction  - do a BEGIN transaction
//    commitTransaction - do a COMMIT transaction
//    setDatestyle      - do SET DATESTYLE TO ISO
//    asciiResult       - Fetch an ascii field and clean up
//    getResult         - do a SELECT on the database

#include <stdio.h>
#include <string.h>
#include "/usr/include/libpq-fe.h"

void exitNicely( void );

extern PGconn *conn;

// startTransaction - BEGIN the transaction

void startTransaction( void )
{
  PGresult *res;

  /* start a transaction block */
  res = PQexec(conn, "BEGIN");
  if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
      fprintf(stdout, "<P>BEGIN command failed<BR>\n");
      fprintf(stdout, "%s</P>\n", PQerrorMessage(conn));
      PQclear(res);
      exitNicely();
    }
  PQclear(res);
}

// commitTransaction - COMMIT TRANSACTION on the MARC database

void commitTransaction( void )
{
  PGresult *res;

  /* commit the transaction */
  res = PQexec(conn, "COMMIT TRANSACTION");
  PQclear(res);
}

// setDatestyle - Perform SET DATESTYLE TO 'ISO' on MARC database

void setDatestyle( void )
{
  PGresult *res;

  res = PQexec(conn,"SET DATESTYLE TO 'ISO'");
  if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
      fprintf(stdout, "<P>SET DATESTYLE command failed<BR>\n");
      fprintf(stdout, "%s</P>\n", PQerrorMessage(conn));
      PQclear(res);
      exitNicely();
    }
  PQclear(res);
}

void asciiResult( PGresult *res, char *szResBuffer, int nLen, int nRow, int nCol )
{
  char szWorkBuffer[4096];
  char *p;

  memset(szWorkBuffer,0,sizeof(szWorkBuffer));
  memcpy(szWorkBuffer,PQgetvalue(res,nRow,nCol),PQgetlength(res,nRow,nCol));
  szWorkBuffer[nLen]='\0';
  p = szWorkBuffer+strlen(szWorkBuffer)-1;
  while ( *p==' ' && p>szWorkBuffer )
    {
      *p = '\0';
      p--;
    }
  strcpy(szResBuffer,szWorkBuffer);
}

// Get results from the database

PGresult *getResults( char *szCursor, char *szSelect, char *szFrom, char *szWhere, char *szOrder )
{
  PGresult *res;
  char szCommand[2048];
  char szTemp[128];

  if ( szWhere )
    sprintf(szCommand,"DECLARE %s CURSOR FOR SELECT %s FROM %s WHERE %s ORDER BY %s",
	    szCursor,szSelect,szFrom,szWhere,szOrder);
  else
    sprintf(szCommand,"DECLARE %s CURSOR FOR SELECT %s FROM %s ORDER BY %s",
	    szCursor,szSelect,szFrom,szOrder);

  // printf("%s\n",szCommand);

  res = PQexec(conn, szCommand);
  if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
      fprintf(stdout, "<P>DECLARE CURSOR %s command failed<BR>\n",szCursor);
      fprintf(stdout, "%s</P>\n", PQerrorMessage(conn));
      PQclear(res);
      exitNicely();
    }
  PQclear(res);

  sprintf(szTemp,"FETCH ALL IN %s",szCursor);
  res = PQexec(conn, szTemp);
  if (PQresultStatus(res) != PGRES_TUPLES_OK) 
    {
      fprintf(stderr, "<P>%s command didn't return tuples properly<BR>\n",szCursor);
      fprintf(stdout, "%s</P>\n", PQerrorMessage(conn));
      PQclear(res);
      exitNicely();
    }

  return res;
}

