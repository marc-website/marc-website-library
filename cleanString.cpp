// cleanString.cpp
//
// cleanString is called by MARC routines which do PQgetvalue operations
// on the MARC database.  cleanString ensures that the result has a
// terminating null then removes trailing blanks.

#include <stdio.h>
#include <string.h>

void cleanString( char *szWork, int nMaxLen )
{
  char *p;

  szWork[nMaxLen]='\0';
  p = szWork+strlen(szWork)-1;
  while ( *p==' ' && p>szWork )
    {
      *p = '\0';
      p--;
    }
}

